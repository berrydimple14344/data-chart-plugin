require.config({
  paths : {
    //-- jQuery Core
    'jquery'          : 'lib/jquery',
    'jquery.ui'       : 'lib/jquery-ui-1.10.1.custom',
    'jquery.plugins'  : 'lib/jquery.plugins',

    //-- System Core
    'underscore'            : 'lib/underscore',
    'underscore.string'     : 'lib/underscore.string',
    'backbone'              : 'lib/backbone',
    'relational'            : 'lib/backbone-relational',
    'marionette'            : 'lib/backbone.marionette',
    'backbone.localStorage' : "lib/backbone.localStorage",
    'backbone.paginator'    : 'lib/backbone.paginator',

    //-- UI Core
    'bootstrap'    : 'lib/bootstrap',

    //-- Et cetera
    'ZeroClipboard': 'lib/ZeroClipboard',
    'modernizr'    : 'lib/modernizr',
    'tpl'          : 'lib/tpl',
    'd3'           : 'lib/d3',
    'datachart'    : 'lib/datachart',
    'b64'          : 'lib/webtoolkit.base64'
  },
  shim : {
    'd3' : {
      exports : 'd3'
    },
    'underscore' : {
      exports : '_'
    },
    'relational' : {
      exports : 'Relational',
      deps: ['backbone', 'underscore']
    },
    'backbone' : {
      exports : 'Backbone',
      deps : ['jquery', 'underscore']
    },
    'underscore.string': {
        deps: ['underscore'],
        exports: '_s'
    },
    'bootstrap' : {
      exports : 'bootstrap',
      deps    : ['jquery']
    },
    'marionette' : {
      exports : 'Backbone.Marionette',
      deps : ['backbone', 'relational']
    }
  },
  deps : ['jquery', 'underscore', 'underscore.string']
});

require(
    ['app', 'backbone', 'routers/index', 'controllers/index'],
    function(app, Backbone, Router, Controller) {
  'use strict';
  app.start();
  new Router({
    controller : Controller
  });

  if(location.hostname==='localhost') {
    Backbone.history.start({ pushState: true });
  } else {
    Backbone.history.start({ root: '/data_chart_2/' });
  }

});
