define(['marionette', 'vent',
        
        'views/Chart/Index', 'views/Browser/Index', 'views/Chart/Options/Index',
        'models/Dataset', 'models/DisplaySettings',
        'collections/DatasetList', 'collections/DisplaySettingsList', 'collections/BrowseList',
        'extensions/ModalRegion', 'extensions/Settings', 'extensions/Utils',

        'modernizr'],
  function(marionette, vent, 

          Chart, Browser, Options,
          Dataset, DisplaySettings,
          DatasetList, DisplaySettingsList, BrowseList,
          ModalRegion, Settings, Utils, 

          modernizr) {
  'use strict';

  var app = new marionette.Application(),
      datasetList = new DatasetList(),
      displaysettingsList = new DisplaySettingsList(),
      browseList = new BrowseList([], { datasets: datasetList }),

      viewOptions = {
        collection: datasetList
      }

  app.addRegions({
    main : '#window',
    modal  : ModalRegion,
  });

  app.addInitializer(function() {
    //-- If a model is pushed into the datasetList collect display it right away
    this.bindTo(datasetList, 'add', this.render, this);
  });

  //-- Call this to redraw the app with the latest model (defaults to chart view)
  app.render = function() {
    viewOptions['model'] = datasetList.at(0);
    console.log("Model to render: ", viewOptions['model']);
    app.main.show( new Chart(viewOptions) )
  };

  app.loadRequestData = function(entrez_id, dataset_id) {
    var self = this,
        reporters = [],
        probesets = [];

    //-- Get a list of all the reporters for this entrez_id & dataset_id combination
    $.getJSON('http://mygene.info/gene/'+ entrez_id +'/?filter=entrezgene,reporter,refseq.rna', function(d) {
      reporters.push( d.refseq.rna );
      _.each(_.keys(d.reporter).reverse(), function(key) { reporters.push( d.reporter[key] ); });

      //-- Refine the reporters to get the probeset list
      $.getJSON( Utils.proxy('dataset/'+ dataset_id +'/values/?format=json&callback=?&reporters='+ _.flatten(reporters).join(',')), function(d) {

        //-- If the dataset_id doesn't belong to the entrez_id
        if(d.probeset_list.length===0) {
          //-- This is when our dataset is not for the tax_id of our entrez_id
          Backbone.history.navigate('/'+ entrez_id , {trigger: true});
        }

        //-- Build the list of our clean reporters and probesets
        _.each(d.probeset_list, function(obj) { probesets.push( _.keys(obj) ); });
        reporters = _.flatten(reporters);
        probesets = _.flatten(probesets);

        //-- If the browser doesn't support SVG or Localstorage redirect them to the old version
        //-- do this *now* so that we can pass them the confirmed dataset_id in addition to the entrez_id (TODO)
        if(Modernizr.svg === false || Modernizr.localstorage === false) {
          //-- Make sure to pass it the TWO url params
          window.location = 'http://plugins.biogps.org/data_chart/data_chart.cgi?id='+entrez_id;
        }

        //-- At this point we have everything we need and know we can draw
        $.getJSON( Utils.proxy( 'dataset/d3/'+ dataset_id +'/'+ probesets[0] +'/?format=json&callback=?' ), function(d3_data) {
          var model = new Dataset({
            //-- Meta & Data is all we need from the "values api"
            meta : d3_data.meta,
            data : d3_data.data,
            //-- Save our other information with the Dataset model to use later
            entrez_id : entrez_id,
            dataset_id : dataset_id,
            reporters : reporters,
            probesets : probesets
          });
          var display_settings = new DisplaySettings({parentDataset: model});
          datasetList.create(model)
          displaysettingsList.create(display_settings);

        });

      });
    });
  };

  //
  //-- Events to trigger application navigation
  //
  vent.on('navigate:browser', function(param) {
    //-- Load the browser app
    viewOptions['collection'] = browseList;
    app.main.show( new Browser(viewOptions) );
  });

  vent.on('dataset:toggle', function(model) {
    viewOptions['model'] = model;
    app.main.show( new Chart(viewOptions) );
  });

  //
  //-- Events to trigger Chart based options
  //
  vent.on('navigate:modal:factor:option', function(params) {
    //-- params includes the model and the factor to show
    app.modal.show( new Options(params) )
  });

  //
  //-- Events to trigger dataset loading and viewing
  //
  vent.on('dataset:new', function(arr) {
    //-- Call this when we change probesets in the header
    var model = arr[0],
        probeset = arr[1];

    //-- Clone doesn't inherent relationships
    var created_model = model.clone();
    $.getJSON( Utils.proxy( 'dataset/d3/'+ model.get('dataset_id') +'/'+ probeset +'/?format=json&callback=?' ), function(d3_data) {
      created_model.set('data', d3_data.data);
      created_model.set('meta', d3_data.meta);
      created_model.set('probeset', probeset);
      created_model.set('updated', Date.now());
    });

    displaysettingsList.create({parentDataset: created_model});

    console.log("Cloned Model: ", created_model);
    datasetList.add(created_model);
  });

  vent.on('dataset:load', function(arr_params) {
    //-- This function is will hit itself potentially twice if the request doesn't include the dataset_id on the first request
    var self = this,
        entrez_id = Number(arr_params[0]),
        dataset_id = arr_params[1];

    if( dataset_id === undefined ) {
      //-- If you've viewed this entrez_id before, what dataset_id
      //-- were you looking at?
        // previous_ds_id = this.collection.find where entrez_id = entrez_id get first.ds_id
        // Backbone.history.navigate('/'+ entrez_id +'/'+ previous_ds_id, {trigger: true});

      //-- If never viewed entrez_id before, get the default dataset_id
      $.getJSON('http://mygene.info/gene/'+ entrez_id +'/?filter=homologene', function(mygene_res) {
        //-- this next line fails if none are found for self id, need to then show datachart browswer
        var taxonomy_id_from_entrez_id = _.find(mygene_res.homologene.genes, function(arr) { return arr[1] === entrez_id; })[0];
        var species_obj = _.find(Settings.species, function(obj) { return obj.taxonomy_id === taxonomy_id_from_entrez_id; });
        console.log('boo', entrez_id, species_obj.dataset_id);
        Backbone.history.navigate('/'+ entrez_id +'/'+ species_obj.dataset_id, {trigger: true});
      });

    } else {
      //-- If we know the entrez_id & dataset_id just go ahead and download their data values
      app.loadRequestData( entrez_id, Number(dataset_id) );
    }
  });

  return app;
});

