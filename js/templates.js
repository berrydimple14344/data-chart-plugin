define(function(require) {
  'use strict';
  return {

    chart : {
      index  : require('tpl!templates/chart/index.tmpl'),

      header  : require('tpl!templates/chart/header.tmpl'),
      footer  : require('tpl!templates/chart/footer.tmpl'),

      views : {
        graph   : require('tpl!templates/chart/views/graph.tmpl'),
        image   : require('tpl!templates/chart/views/image.tmpl'),
        _export : require('tpl!templates/chart/views/export.tmpl'),
      },

      modal : {
        options_list      : require('tpl!templates/chart/modal/optionsList.tmpl'),
        options_list_item : require('tpl!templates/chart/modal/optionsListItem.tmpl'),
      }

    },

    browser : {
      index    : require('tpl!templates/browser/index.tmpl'),

      controls : require('tpl!templates/browser/controls.tmpl'),
      item     : require('tpl!templates/browser/item.tmpl')
    }

  }
});
