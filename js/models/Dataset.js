define(['backbone', 'models/DisplaySettings', 'collections/DisplaySettingsList',
        'extensions/Factors', 'd3', 'datachart' ], 
        function(Backbone, DisplaySettings, DisplaySettingsList,
                factors){
  'use strict';

  return Backbone.RelationalModel.extend({
    defaults: {
      meta             : {},
      data             : [],

      entrez_id        : 0,
      dataset_id       : 0,

      reporters        : [],
      probeset         : '',
      probesets        : [],

      cached_data      : [],

      factor_clr_scale : d3.scale.category20(),
      clr_i            : 0,

      viz              : {},
      html             : '',

      created          : 0,
      updated          : 0,
      show_image       : false,
      aggregated       : false,

      search        : '',
      display_text  : 'title',

      size : { height: 600, width: 400 },
    },

    relations: [
      {
        type: 'HasOne',
        key: 'DisplaySettings',

        relatedModel: DisplaySettings,
        collectionType: DisplaySettingsList,

        reverseRelation : {
          key : 'parentDataset',
          type : 'HasOne',
          includeInJSON: true
        }
      }
    ],

    initialize : function() {
      if ( this.isNew() ) {

        if(this.get('probeset')=='') {
          this.set('probeset', this.get('probesets')[0] )
        }

        // this.set('cached_data', $.extend(true, {}, this.get('data')) )
        this.set('cached_data', this.get('data') );
        this.set('created', Date.now());
        this.set('updated', Date.now());

        //-- Setup the graph
        this.set('viz', new DataChart())
        this.get('viz').dataset = this.get('data');

        this.get('viz').display_text = this.get('display_text');
        this.get('viz').size = this.get('size');
        this.get('viz').animate_speed = 400;

        //-- Setup the event listeners
        this.bind('change:data',          this.updateData);
        this.bind('change:search',        this.updateSearch);
        this.bind('change:display_text',  this.updateDisplayText);
      }
    },

    validate : function(attrs) {
      var val;
      _.each(attrs.data, function(sample) {
        _.each(_.keys(sample['factors']), function(factor) {
          val = sample['factors'][ factor ];
          delete sample['factors'][ factor ];
          sample['factors'][ _.str.slugify(factor) ] = val;
        });
      });
    },

    updateData : function() {
      console.log('Dataset', 'updateData');
      this.get('viz').data = this.get('data');
    },

    updateSearch : function() {
      console.log('Dataset', 'updateSearch');
      this.get('viz').searchHighlight( this.get('search') );
    },

    updateDisplayText : function() {
      console.log('Dataset', 'updateDisplayText');
      // var viz = this.get('viz');
      // viz.display_text = this.get('display_text');
      // viz.refresh();
      // viz.refresh();
    },

  });
});
