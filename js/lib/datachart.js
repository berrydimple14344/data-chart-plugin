//-- Create the DataChart object via construtor function
function DataChart(options) {

  var self = this;

  function getWidestLabel() {
    var arr = [],
        width;
    _.each( $("text.legenedHLabel"), function(v) {
      arr.push(v.getBBox().width);
    });
    width = _.max(arr);
    if (width === Number.NEGATIVE_INFINITY) { width = 80; }
    return width+2;
  };

  function getFactorsWidth() {
    var factorColumn = self.$el.find('g.yAxisFactors')[0] || false,
        width;
    factorColumn ? width = factorColumn.getBBox().width : width = 0;
    return width+2;
  }

  function getyAxisTextHeight() {
    var textBox = $('g.legenedHLabels text')[0] || false,
        height;
    textBox ? height = textBox.getBBox().height : height = 0;
    return height;
  }

  function getSampleColor(s) {
    var val = s['factors'][_.last(self.display.order)];
    return self.display.colors[_.last(self.display.order)][val] || '#708090';
  }

  function determineAnimate(animate) {
    if(animate) {
      return self.animate_speed;
    } else { return 0; }
  }

  function layoutCalc() {
    var hLabel_width = getWidestLabel();
    //-- This performs the calculations required to layout the graph, margins, footers, etc in a way using groups that allow them to scale. The idea being that each value is generated dynamically
    var ds = self.dataset,
        view_size = $(window).height() - Math.floor($('#header').height()*1.1);

    if( (view_size / ds.length) <= 14 ) {
      view_size = ds.length*14;
    }
    self.dimensions = {
      'paper' : {
        'height' : view_size,
        'width' : self.size.width,
        'margin' : 10
      }
    };
    var dim = self.dimensions;
    dim.yAxis = {
      'height' : dim.paper.height - ( dim.paper.margin * 2 ),
      'width' : getFactorsWidth() + hLabel_width,
    };
    dim.xAxis = {
      'header' : {
        'height' : dim.paper.margin * 2,
        'width' : dim.paper.width - ( dim.paper.margin * 2 ) - dim.yAxis.width,
      },
      'footer' : {
        'height' : dim.paper.margin * 2,
        'width' : dim.paper.width - ( dim.paper.margin * 2 ) - dim.yAxis.width,
      }
    };
    dim.view = {
      'height' : dim.paper.height - ( dim.paper.margin * 2 ) - dim.xAxis.header.height - dim.xAxis.footer.height,
      'width' : dim.paper.width - ( dim.paper.margin * 2 ) - dim.yAxis.width,
    };
  };

 function initialize(options) {
    if (!options) options = {};

    //-- Public
    self.$el = options.$el || $('div.datachart');
    self.size = {width: 0, height: 0};
    self.dataset = options.dataset || [];
    self.display_text = options.display_text || 'title';
    self.animate_speed = 400;

    self.display = {}; //-- Lookup hash for order of factor options

    //-- Private
    self.xScale; //-- X scaling factor
    self.yScale; //-- Y scaling factor
    self.mean = 0; //-- Current mean value for displayed sample values
    self.tooltip;
    self.infotip;
  };

  //
  // -- Utility functions for various routines
  //
  function calculateMean(arr) {
    return _.reduce(arr, function(memo, num) {
      return memo + num;
    }, 0) / arr.length;
  };

  function calculateXScale() {
    var maxMean = _.max( _( self.dataset ).pluck('error') ) || 0,
        minMean = _.min( _( self.dataset ).pluck('error') ) || 0,
        values = _.pluck( self.dataset , 'value');

    if(maxMean===Number.NEGATIVE_INFINITY){maxMean=0;};
    if(minMean===Number.NEGATIVE_INFINITY){minMean=0;};
    if(maxMean===Number.POSITIVE_INFINITY){maxMean=0;};
    if(minMean===Number.POSITIVE_INFINITY){minMean=0;};

    self.xScale = d3.scale.linear()
        .domain([
                (function() {
                  if(_.min(values) < 0) {
                    return (_.min(values) - Math.abs(minMean))* (1-self.display.zoom.low);
                  } else {
                    return (_.max(values) * self.display.zoom.low);
                  }
                })(),

                 (_.max(values) + maxMean)*self.display.zoom.high
                 ])
        .range([0, self.dimensions.view.width]);

    self.xAxis = d3.svg.axis()
        .scale(self.xScale)
        .orient('top')
  };

  function calculateYScale() {
    self.yScale = d3.scale.ordinal()
      .domain( _.range(self.dataset.length) )
      .rangeRoundBands([0, self.dimensions.view.height], 0.25);
  };

  function calculateAxes() {
    calculateXScale();
    calculateYScale();
    self.mean = calculateMean( _.pluck( self.dataset, 'value') );
  };

  //
  //-- Drawing functions with placing data on screen
  //
  function drawXLegend() {
    self.xAxisHeader
      .style('fill', 'none')
      .call(self.xAxis);

    var xGridAttrs = { 'transform' : 'translate(0,'+ self.yScale(self.dataset.length) +')',
                       'fill' : '#ccc',
                       'font-size' : '12px',
                       'font-family' : '"Helvetica Neue", Helvetica, Arial, "Lucida Grande", sans-serif', };

    self.xAxisHeader
      .append('g')
        .attr('class', 'guide_lines_x')
        .call( self.xAxis.tickFormat('').tickSize(-self.dimensions.view.height,0,0));

    var xAxisHeaderStyles = { 'fill' : '#000',
                              'font-size' : 12,
                              'font-family' : '"Helvetica Neue", Helvetica, Arial, "Lucida Grande", sans-serif'};
    self.xAxisHeader.selectAll('g.xAxis.header g text').style(xAxisHeaderStyles);

    var xAxisTickLineStyles = { 'stroke' : '#CCC', };
    self.xAxisHeader.selectAll('g.guide_lines_x g line').style(xAxisTickLineStyles);
  };

  function drawYLegend() {
    var infotips = d3.selectAll('div.infotip').remove();
    self.infotip = d3.select('body')
      .append('div')
        .attr('class', 'infotip')
        .style('position', 'absolute')
        .style('z-index', '10')
        .style('visibility', 'hidden');

    self.yAxisLabels
      .attr('transform', 'translate('+ getFactorsWidth() +', 0)');

    var legenedHLabel = self.yAxisLabels
        .selectAll('.legenedHLabel')
          .data(self.dataset);

    var col_width = getWidestLabel(),
        font_size = Math.floor( self.yScale.rangeBand() );
        if(font_size >= 18) { font_size = 18; }
        if(font_size <= 2 ) { font_size = 2; }

    var legenedHLabelAttrs = { 'class' : 'legenedHLabel',
                               'y' : function(d, i) { return self.yScale(i) + (self.yScale.rangeBand()/2)  },
                               'fill' : '#333',
                               'width' : function(d) {
                                 if( d[self.display_text] === undefined ) {
                                   return 0;
                                 } else {
                                   return col_width;
                                 }
                               },
                               'dominant-baseline' : 'central',
                               'text-anchor' : 'right',
                               'font-size' : font_size,
                               'font-family' : '"Helvetica Neue", Helvetica, Arial, "Lucida Grande", sans-serif', };

    legenedHLabel.enter()
      .append('text')
        .attr(legenedHLabelAttrs)
        .text(function(d) { if( self.display_text === 'value') {
                              return Math.round(d[self.display_text]*100)/100;
                            } else {
                              return d[self.display_text] || '';
                            }
              });
    legenedHLabel.exit()
      .remove();
    legenedHLabel
      .attr(legenedHLabelAttrs)
        .text(function(d) { if( self.display_text === 'value') {
                              return Math.round(d[self.display_text]*100)/100;
                            } else {
                              return d[self.display_text] || '';
                            }
        });

    legenedHLabel.on('mouseover', function() { return self.infotip.style('visibility', 'visible'); })
                 .on('mousemove', function(obj) {
                     return self.infotip
                       .style('top', (event.pageY-16)+'px')
                       .style('left', (event.pageX+10)+'px')
                        .html(function() {
                          var o = obj['factors'], str = '<h3>'+ obj[self.display_text] +'</h3><ol>';
                          _.each(_.keys(o),function(v){ str+='<li><strong>'+ _.str.titleize(v) +':</strong> '+ _.str.titleize( String(o[v]) ) +'</li>'; });
                          return str+='</ol>';
                        });
                }).on('mouseout', function() { return self.infotip.style('visibility', 'hidden'); });

  };

  function drawYLegendFactorGroups() {
    //-- Should eventually change this loop to .data using the shared_factors array
    var col_width = getWidestLabel();
    var factorGroup = self.yAxisFactors
      .selectAll('g')
        .data( self.display.order );

    var factorGroupAttrs = {
      'class' : function(d) { return 'factorBars '+d; },
      'height' : self.dimensions.yAxis.height,
      'width' : 24,
      'opacity' : function(factor) {
        if ( self.display.shown[factor]===false ) {
          return 0.0;
        } else {
          return 1;
        }
      },
      'transform' : function(d, i) { return 'translate('+ ( i*28 ) +', 0)' },
    }

    factorGroup.enter()
      .append('g')
      .attr(factorGroupAttrs);
    factorGroup.exit()
      .remove();
    factorGroup
      .attr(factorGroupAttrs).each( drawYLegendFactors );
  };

  function drawYLegendBoundingBox(fctr, findex, that) {
    var agg = [],
        start_index = 0,
        groups = [];
    _.each(self.dataset, function(d, i) {
      if ( _.isFunction(d) === false ) {
        var sample_f0_type = d['factors'][ self.display.order[findex] ],
            option_index = self.display.colors[fctr][sample_f0_type];
        agg.push({ 'hex': option_index,
                    'opt' : sample_f0_type });
      }
    });

    _.each(agg, function(v, i) {
      if( v.hex !== (agg[i+1] || {}).hex ) {
        //-- if the next value is different from the current
        groups.push({ 's': start_index, 'e': i, 'fill': v.hex, 'opt': v.opt  })
        start_index = i+1;
      }
    })

    // This method doesn't work as they're not unique if in a 2nd tier factor type
    // _.each(_.uniq(agg), function(v) {
    //   groups.push( { 'i': _.indexOf(agg, v), 'e': _.lastIndexOf(agg, v) } )
    // });

    var yAxisBoundBoxs = d3.select(that).selectAll('rect.factorBarGrouped').remove();
    _.each(groups, function(group) {
      var yAxisBoundBoxsAttrs = { 'class' : 'factorBarGrouped',
                              'width' : 24,
                              'y' : function() { return self.yScale( group.s ) },
                              'x' : 0,
                              'height' : function() { 
                                if(group.s === group.e) {
                                  //-- only one bar high
                                  return self.yScale.rangeBand();
                                } else {
                                  return (self.yScale( group.e )+self.yScale.rangeBand()) - self.yScale( group.s );
                                }
                              },
                              'fill' : function() { return group.fill; }
                            };
      var yAxisBoundBoxs = d3.select(that).append('rect').attr(yAxisBoundBoxsAttrs);
      yAxisBoundBoxs.on('mouseover', function(obj) {
        var rect = d3.select(this).attr('fill', d3.rgb( group.fill ).brighter(.2) );
        return self.tooltip.style('visibility', 'visible');
      }).on('mousemove', function(obj) {
        return self.tooltip
          .style('top', (event.pageY-16)+'px')
          .style('left', (event.pageX+10)+'px')
          .text( group.opt );
      }).on('mouseout', function(obj) { 
        var rect = d3.select(this).attr('fill', group.fill );
        return self.tooltip.style('visibility', 'hidden');
      });

    });

  }

  function drawYLegendFactors() {
    var that = this;
      yAxisFactorRect = d3.select(this)
      .selectAll('rect')
        .data(self.dataset);

    var fctr = arguments[0],
        findex = arguments[1];

    var sampleRectAttrs = { 'class' : 'factorBars',
                            'width' : 24,
                            'y' : function(d, i) { return self.yScale(i) },
                            'x' : 0,
                            'height' : self.yScale.rangeBand(),
                            'fill' : function(d, i) {
                              var sample_f0_type = d['factors'][ self.display.order[findex] ];
                              return self.display.colors[fctr][sample_f0_type] || '#708090';
                            }
                          }
    yAxisFactorRect.enter()
      .append('rect')
        .attr(sampleRectAttrs);
    yAxisFactorRect.exit()
      .remove();
    yAxisFactorRect.transition()
      .duration(self.animate_speed)
        .attr(sampleRectAttrs);

   //-- These only show the "combined" mouse over panel
   yAxisFactorRect.on('mouseover', function(obj) {
     return self.infotip.style('visibility', 'visible');
   }).on('mousemove', function(obj) {
     return self.infotip
       .style('top', (event.pageY-16)+'px')
       .style('left', (event.pageX+10)+'px')
       .html(function() {
         var o = obj['factors'][fctr],
             str = '<h3>Combined:</h3><ol>',
             more = false;
         _.each(o, function(arr, i) { 
           if(i <= 2) {
            str+='<li><strong>'+ arr[0] +':</strong> n = '+ arr[1] +';</li>';
           } else { more = true; }
         });
         if(more) { str+='<li>more...</li>'; }
         return str+='</ol>';
       });
  }).on('mouseout', function(obj) { 
     return self.infotip.style('visibility', 'hidden');
   });

    drawYLegendBoundingBox(fctr, findex, that);
  }

  function drawSampleBars(animate) {
    var tooltips = d3.selectAll('div.tooltip').remove();
    self.tooltip = d3.select('body')
      .append('div')
        .attr('class', 'tooltip')
        .style('position', 'absolute')
        .style('z-index', '10')
        .style('visibility', 'hidden');

    var sampleRect = self.view
      .selectAll('rect')
        .data(self.dataset);

    var sampleRectAttrs = { 'class' : 'sampleBars',
                            'y' : function(d, i) { return self.yScale(i) },
                            'x' : function(d, i) {
                              var x;
                              if( d.value >= 0 ) {
                                x = self.xScale(0);
                                if ( x >= self.dimensions.view.width ) { x = self.dimensions.view.width; }
                              } else {
                                x = self.xScale(d.value);
                              }
                              if (x <= 0) { x = 0; }
                              return x;
                            },
                            'width' : function(d) {
                              var w,
                                  xscalebase = self.xScale(0),
                                  s = 0;

                              if( d.value >= 0) {
                                if( xscalebase >= 0 ) {
                                  s = xscalebase;
                                };
                                w = (self.xScale(d.value) - s);
                                if( (w + s) >= self.dimensions.view.width ) {
                                  w = self.dimensions.view.width - s;
                                }
                              } else {
                                w = xscalebase;
                              }

                              if(w <= 0 ) { w = 0; }
                              return w;
                            },

                            'height' : self.yScale.rangeBand(),
                            'fill' : function(d) { return getSampleColor(d); }
                          };
    sampleRect.enter()
      .append('rect')
        .attr(sampleRectAttrs);
    sampleRect.exit()
      .remove();
    sampleRect.transition()
      .duration(determineAnimate(animate))
        .attr(sampleRectAttrs);

    sampleRect.on('mouseover', function(obj) {
                var rect = d3.select(this).attr('fill', d3.rgb( getSampleColor(obj) ).brighter(.2) );
                return self.tooltip.style('visibility', 'visible');
              })
              .on('mousemove', function(obj) {
                return self.tooltip
                  .style('top', (event.pageY-16)+'px')
                  .style('left', (event.pageX+10)+'px')
                  .text( Math.round(obj.value*100)/100 );
              })
              .on('mouseout', function(obj) { 
                var rect = d3.select(this).attr('fill', getSampleColor(obj) );
                return self.tooltip.style('visibility', 'hidden');
              });
    drawErrorBars(animate);
  };

  function drawErrorBars(animate) {
    //Time to draw the error bars
    var vErrorBarTicks = self.view
      .selectAll('line.verticalError')
        .data(self.dataset);

    var vX = function(obj, error) {
      var xpos = 0;

      if( obj.value >= 0 ) {
        //-- If POS
        xpos = self.xScale(obj.value);
        if(error && obj.error) {
          //-- OUT REACHING POINT
          xpos += (self.xScale( obj.error ) - self.xScale(0) );
        }
      } else {
        //-- If NEG
        xpos = self.xScale(obj.value);
        if( error && obj.error ) {
          //-- OUT REACHING POINT
          xpos -= Math.abs(self.xScale( obj.error ) )
        }
      }

      //-- Cleanup the bounds
      if (xpos >= self.dimensions.view.width) { xpos = self.dimensions.view.width; }
      if (xpos <= 0 ) { xpos = 0; }
      return xpos;
    }

    var vErrorBarTicksAttrs = { 'class' : 'verticalError',
                                'x1' : function(d, i) { return vX(d, true) },
                                'x2' : function(d, i) { return vX(d, true) },

                                'y1' : function(d, i) { return self.yScale( i ) },
                                'y2' : function(d, i) { return self.yScale( i ) + self.yScale.rangeBand(); }
                              };
    var vErrorBarTicksStyles = { 'stroke' : 'rgb(224,154,37)',
                                 'stroke-width' : function(d) { if(d.error === 0) { return 0; } else { return 1; } } };
    vErrorBarTicks.enter()
      .append('svg:line')
        .attr(vErrorBarTicksAttrs)
        .style(vErrorBarTicksStyles);
    vErrorBarTicks.exit()
      .remove();
    vErrorBarTicks.transition()
      .duration(determineAnimate(animate))
        .attr(vErrorBarTicksAttrs)
        .style(vErrorBarTicksStyles);

    var hErrorBarTicks = self.view
      .selectAll('line.horizontalError')
        .data(self.dataset);

    var hErrorBarTicksAttrs = { 'class' : 'horizontalError',
                                'x1' : function(d, i) { return vX(d, false); },
                                'y1' : function(d, i) { return self.yScale( i ) + (self.yScale.rangeBand()/2); },
                                'x2' : function(d, i) { return vX(d, true) },
                                'y2' : function(d, i) { return self.yScale( i ) + (self.yScale.rangeBand()/2); }
                              }
    var hErrorBarTicksStyles = { 'stroke' : 'rgb(224,154,37)',
                                 'stroke-width' : function(d) { if(d.error === 0) { return 0; } else { return 1; } } };
    hErrorBarTicks.enter()
      .append('svg:line')
        .attr(hErrorBarTicksAttrs)
        .style(hErrorBarTicksStyles);
    hErrorBarTicks.exit()
        .remove();
    hErrorBarTicks.transition()
      .duration(determineAnimate(animate))
        .attr(hErrorBarTicksAttrs)
        .style(hErrorBarTicksStyles);
  };

  function drawMean() {
    self.view
      .selectAll('line.meanBar').remove();
    self.xAxisFooter
      .selectAll('text.mean').remove();

    if (self.xScale(self.mean) < self.dimensions.view.width && self.xScale(self.mean) > 0 ) {

    var meanBar = self.view
      .selectAll('line.meanBar')
        .data([self.mean])
    var meanBarAttrs = { 'class' : 'meanBar',
                         'x1' : function() { return self.xScale(self.mean) },
                         'x2' : function() { return self.xScale(self.mean) },
                         'y1' : 0,
                         'y2' : self.dimensions.view.height,
                         'stroke' : '#333',
                         'stroke-width' : .5
                       };
    meanBar.enter()
      .append('line')
        .attr(meanBarAttrs);
    meanBar.exit()
      .remove();
    meanBar
        .attr(meanBarAttrs)

    var meanLabel = self.xAxisFooter
      .selectAll('text')
        .data([self.mean])
    var meanLabelAttrs = { 'class' : 'mean',
                           'x' : function() { return self.xScale(self.mean) },
                           'y' : 0,
                           'dy' : 14,
                           'text-anchor' : 'middle',
                           'fill' : '#333',
                           'font-size' : '12px',
                           'font-family' : '"Helvetica Neue", Helvetica, Arial, "Lucida Grande", sans-serif'
                         };
    meanLabel.enter()
      .append('text')
        .attr(meanLabelAttrs)
        .text( Math.round(self.mean*100)/100 );
    meanLabel.exit()
      .remove();
    meanLabel
        .attr(meanLabelAttrs);
    }
  };

  //
  //-- DataChart public functions responsible for performing core
  //-- actions like init, calc, refreshing and drawing
  //
  this.searchHighlight = function(needle) {
    $.each( $('svg g.yAxis text.legenedHLabel') , function(i, v) {
      var haystack = v.textContent.toLowerCase().trim(), fill;
      ( (needle == haystack.substring(0, needle.length) || haystack.indexOf(needle) != -1 ) && needle.length > 0) ? fill = 'steelblue' : fill = '#333';
      d3.select(v).attr('fill', fill)
    });
    return this;
  };

  this.refresh = function() {
    var dim = self.dimensions;
    //-- Recalc all the positioning values, run drawYLegend before
    //-- so we know the width of the text
    drawYLegend();
    layoutCalc();
    //-- Update the view elements with the new position values
    var paperAttrs = { 'width' : dim.paper.width,
                       'height' : dim.paper.height, };
    self.paper.attr(paperAttrs);

    var yAxisAttrs = { 'height' : dim.yAxis.height,
                       'width' : dim.yAxis.width,
                       'transform' : 'translate('+ dim.paper.margin +', '+ (dim.paper.margin + dim.xAxis.header.height) +')', };
    self.yAxis = self.yAxis.attr(yAxisAttrs);

    var xAxisHeaderAttrs = { 'height' : dim.xAxis.header.height,
                             'width' : dim.xAxis.header.width,
                             'transform' : 'translate('+ (dim.paper.margin + dim.yAxis.width) +', '+ (dim.paper.margin + dim.xAxis.header.height) +')', }
    self.xAxisHeader = self.xAxisHeader.attr(xAxisHeaderAttrs);

    var xAxisFooterAttrs = { 'height' : dim.xAxis.header.height,
                             'width' : dim.xAxis.header.width,
                             'transform' : 'translate('+ (dim.paper.margin + dim.yAxis.width) +', '+ (dim.paper.margin + dim.xAxis.header.height + dim.view.height) +')', }
    self.xAxisFooter = self.xAxisFooter.attr(xAxisFooterAttrs);

    var viewAttrs = { 'height' : dim.view.height,
                      'width' : dim.view.width,
                      'transform' : 'translate('+ (dim.paper.margin + dim.yAxis.width) +', '+ (dim.paper.margin + dim.xAxis.header.height) +')', }
    self.view = self.view.attr(viewAttrs)

    //-- Update all the axis calculations and redraw
    calculateAxes()
    //- Top most get drawn on top of by bottom most
    drawXLegend();
    drawYLegend();
    drawYLegendFactorGroups();
    drawMean();
    drawSampleBars(true);

    return this;
  };

  //-- First thing to run
  this.init = function() {
    console.log("d3 chart init");
    layoutCalc();
    calculateAxes(); 
    return this;
  };

  //-- Second thing to run
  this.drawWindowGroups = function() {
    var dim = self.dimensions;
    //-- Insert the layout groups into the svg element for the first time / only run this once on initial draw to place in DOM
    var paperAttrs = { 'class' : 'paper',
                       'width' : dim.paper.width,
                       'height' : dim.paper.height, };

    self.paper = d3.select( self.$el[0] )
      .append('svg')
        .attr(paperAttrs);

    var yAxisAttrs = { 'class' : 'yAxis',
                       'height' : dim.yAxis.height,
                       'width' : dim.yAxis.width,
                       'transform' : 'translate('+ dim.paper.margin +', 0)', };
    self.yAxis = self.paper
      .append('g')
        .attr(yAxisAttrs);

    self.yAxisFactors = self.yAxis
      .append('g')
        .attr('class', 'yAxisFactors')
    self.yAxisLabels = self.yAxis
      .append('g')
        .attr('class', 'legenedHLabels')
        .attr('transform', 'translate('+ getFactorsWidth() +', 0)');

    var xAxisHeaderAttrs = { 'class' : 'xAxis header',
                             'height' : dim.xAxis.header.height,
                             'width' : dim.xAxis.header.width,
                             'transform' : 'translate('+ (dim.paper.margin + dim.yAxis.width) +', '+ (dim.paper.margin + dim.xAxis.header.height) +')', }
    self.xAxisHeader = self.paper
      .append('g')
        .attr(xAxisHeaderAttrs);

    var xAxisFooterAttrs = { 'class' : 'xAxis footer',
                             'height' : dim.xAxis.header.height,
                             'width' : dim.xAxis.header.width,
                             'transform' : 'translate('+ (dim.paper.margin + dim.yAxis.width) +', '+ (dim.paper.margin + dim.xAxis.header.height + dim.view.height) +')',
                               }

    self.xAxisFooter = self.paper
      .append('g')
        .attr(xAxisFooterAttrs)

    var viewAttrs = { 'class' : 'view',
                      'height' : dim.view.height,
                      'width' : dim.view.width,
                      'transform' : 'translate('+ (dim.paper.margin + dim.yAxis.width) +', 0)', }
    self.view = self.paper
      .append('g')
        .attr(viewAttrs);

    return this;
  };

  //-- When only changing the zoom, this prevents other
  //-- layout calculations from having to run
  this.changeZoom = function() {
    calculateXScale();
    drawXLegend();
    drawSampleBars(false);
    drawMean();
    return this;
  };

  initialize(options);

  return this;

}
