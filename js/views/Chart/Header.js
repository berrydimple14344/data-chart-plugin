define(['marionette', 'templates', 'vent',
        'extensions/Utils', 'extensions/Factors', 'jquery.ui'], 
    function (Marionette, templates, vent,
              Utils, factors) {
  'use strict';

  return Marionette.ItemView.extend({
    template : templates.chart.header,

    ui : {
      factors : '#factors ul',
      search  : '.search input',
      zoom    : '#zoom'
    },

    events : {
      'input .search input'        : 'changeSearchText',
      'click #browser-btn'         : 'clickBrowser',

      //-- Drop down select options
      'change select#display_text' : 'changeDisplayText',
      'change select#probesets'    : 'changeProbeset',

      //-- Factor bar events
      'click div.options_sort'     : 'showFactorOptionModal',
      'change input.toggle'        : 'changeShown',
      'change input.aggregate'     : 'changeAggregate',
    },

    onRender : function() {
      var self = this;

      //-- Sliding the zoom
      this.ui.zoom.slider({
        range: true,
        min: 0, max: 1, step: 0.01, values: [ 0, 100 ],
        slide: function( event, ui ) {
          var high_val;
          ( ui.values[0] == ui.values[1] ) ? high_val = ui.values[1]+0.01 : high_val = ui.values[1];
          self.model.get('DisplaySettings').set('zoom', { low: ui.values[0], high: high_val });
          self.model.get('DisplaySettings').trigger('change:zoom');
        }
      });

      //-- Add drag controls to list to order
      this.ui.factors.sortable({
        placeholder: 'ui-state-highlight',
        update: function(event, ui) { self.model.get('DisplaySettings').set('order', $(this).sortable('toArray')); },
      });
      this.ui.factors.disableSelection();
    },

    changeSearchText : function(evt) {
      evt.preventDefault();
      console.log('change text from header');
      this.model.set('search', this.ui.search.val().trim() );
    },

    clickBrowser : function(evt) {
      evt.preventDefault();
      vent.trigger('navigate:browser', '');
    },

    changeDisplayText : function(evt) {
      evt.preventDefault();
      this.model.set('display_text', $(evt.target).val().toLowerCase().trim() );
    },

    changeProbeset : function(e) {
      var probeset = $(e.target).val().toLowerCase().trim();

      //-- Do we already have this dataset id & probset combo?
      var results =  this.collection.where({dataset_id: this.model.get('dataset_id'), probeset: probeset});
      if( results.length === 0 ) {
        vent.trigger('dataset:new', [this.model, probeset]);
      } else {
        this.model = results[0]
        this.model.set('updated', Date.now());
        this.collection.sort();
        vent.trigger('dataset:toggle', this.model);
      }
    },

    showFactorOptionModal : function(evt) {
      evt.preventDefault();
      var $el = $(evt.target),
          attr = $el.closest('li').attr('class').split(" ")[0].toLowerCase().trim(),
          params = {
            model : this.model,
            factor : attr
          }
      vent.trigger('navigate:modal:factor:option', params)
    },

    changeShown : function(evt) {
      evt.preventDefault();
      var $target = $(evt.target),
          factor = $target.parent().attr('class').split(" ")[0].toLowerCase().trim(),
          bool = $target.is(':checked'),
          shown_obj = this.model.get('DisplaySettings').get('shown');
      shown_obj[factor] = bool;
      this.model.get('DisplaySettings').set('shown', shown_obj);
      this.model.get('DisplaySettings').trigger('change:shown')
    },

    changeAggregate : function(evt) {
      evt.preventDefault();
      var $target = $(evt.target),
          factor = $target.parent().attr('class').split(" ")[0].toLowerCase().trim(),
          bool = $target.is(':checked'),
          aggregate_obj = this.model.get('DisplaySettings').get('aggregate');
      aggregate_obj[factor] = bool;
      this.model.get('DisplaySettings').set('aggregate', aggregate_obj);
      this.model.get('DisplaySettings').trigger('change:aggregate')
    }

  });
});
