define(['marionette', 'templates', 'vent',
        'b64'], 
    function (Marionette, templates, vent) {
  'use strict';

  return Marionette.ItemView.extend({
    template : templates.chart.views.image,

    ui : {
      image : 'img'
    },

    onRender : function() {
      var b64 = Base64.encode(this.model.get('html'));
      this.ui.image.attr("src", "data:image/svg+xml;base64,\n"+b64);
    },

  });
});
