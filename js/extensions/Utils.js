define([], 
    function () {
  'use strict';

  return {
    proxy : function(url) {
      return ( document.URL.indexOf('localhost') === -1 ) ? url : 'http://biogps.org/'+ url;
    }
  }
});
