define(['extensions/MathFunctions', 'extensions/Factors'], 
    function (math, factors) {
  'use strict';

  return {
    group : function(found, factor_order) {
      if(found.length===0){ return []; };

      var combine_factors = _.difference( factors.getOrder(_.pluck(found, 'factors')) , factor_order);
      var agg_sample = $.extend(true, {}, found[0]),
          sum_val = 0,
          grouped_vals = _.pluck(found, 'value'),
          grouped_samples = _.pluck(found, 'sample'),
          agg_title = [];

      //-- Give aggregation info for top level info
      agg_sample['sample'] = grouped_samples.join(", ");
      _.each(factor_order, function(v) { agg_title.push(v + ' ('+ agg_sample.factors[v] +')'); });
      agg_sample['title'] = agg_title.join(', ') +"; n = "+ found.length;
      agg_sample['value'] = math.getMean(grouped_vals);
      agg_sample['error'] = math.getStandardError(grouped_vals);

      //-- Modify aggregation info for combined factors which were not
      //-- aggregated
      _.each(combine_factors, function(factor) {
        var combinations = _.sortBy( _.pairs( _.countBy(found, function(f) { return f['factors'][factor]; })) , function(arr) { return -arr[1]; });
        if(combinations.length > 1) {
          agg_sample['factors'][factor] = combinations;
        }
      });
      return agg_sample;
    }
  };
});
