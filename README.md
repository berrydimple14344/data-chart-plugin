# Data Chart Plugin // Version 3 Beta // SVG

## Description

The Data Chart Plugin is a web application that allows the
visualization, filtration, sorting, searching, scaling and hierarchical
organization of gene expression datasets from the
[BioGPS](http://biogps.org/) web service. This plugin is an ongoing
attempt to replicate and enhance the Data Chart Plugin using SVG and a
more clearly designed design pattern.

## Organization

A key part of rebuilding the Data Chart Plugin from scratch was to
easily allow it to be enhanced for better vizualization techniques,
modified for organization purposes, and extensible for easy access by
other developers. This was achieved by isolating the two projects (The )

### [Backbone](http://backbonejs.org/) Application

The BackboneJS application manages the loading and organization of all
the data, the hierarchical organization of all the factors, the dataset
browser and pagination, the single page app windowing system, as well as
the redrawing, rescaling and refreshing of the D3 vizualization. 

### [D3](http://d3js.org/) Vizualization

The D3js drawing application lives in an isolated environment and should
be unaware of it's parent application. This allows the code to
potentially be used on other sites and in other projects. The 3 elements
on the page that the application is aware of is:

* Parent containing DIV in which the SVG will be drawn
* Image element to contain the b64 encoded image of the SVG
* jQuery slider to get back the width information (so they're aligned)

The rest of the application operates through a series of variables
pasted to the DataChart() object during initization or by being updated
during runtime.

The D3 applications depends on the array of sample objects to be
ordered, aggregated, and sorted prior to being passed in as the
vizualization context has no data manipulation capacity of any kind.

#### Public API

Many of the public methods that can be called on the DataChart don't take any parameters but require updated parameters to the DataChart object. This is so that some of the variables can be set, changed, or reverted without having to call the various public methods throughout the drawing or redrawing process.

#### Object variables to set / modify
* $el = $sel
* $header = $sel
* dataset = []

* shared_factors = []
* hidden_factors = []
* shared_factors_obj = {}

* display_text = 'title'
* color_on = 'color_idx'

* zoom = {low: 0.0, high: 1.0}
* animate_speed = 400

#### drawWindowGroups()
 > No parameters

 Only to be called once per Backbone view. This appends in the svg
elements and groups required for drawing.

#### refresh()
 > No parameters

 Call anytime a window resize, datachart manipulation, axis alteration
or other visual change occurs. This recalculates the groups, and updates
the components of a preexisting DataChart.

#### changeZoom()
 > No parameters

 Can be run in suppliment of refresh if it's known that the only visual
elements that need to be updated are those connected to the x-axis
scale. This improves speed by reducing the y-axis alterations to occur
when zooming a chart in or out.

#### searchHighlight(needle)
  > needle : A search string 

 This iterates over the svg context that the current DataChart is aware
of. On removal of a string it'll update itself to revert back to default
coloring. 

#### updateImage($img)
 > $img : A jQuery obj selection for the image element to draw the visualization to

#### Basic Usage

    //-- Instantiate the datachart
    var dc = new DataChart()

This is done only once and it's during the application. 

    //-- Set vars for the DataChart object
    dc.$el = this.$('div.datachart');
    dc.$header = views.header.$el;
    dc.dataset = models.dataset.get('data');

    dc.shared_factors = ST.shared_factors;
    dc.shared_factors_obj = ST.shared_factors_obj;

    dc.display_text = 'title';

    dc.zoom = { low: 0.0, high: 1.0 };
    dc.animate_speed = 400;

Changing these paramters can be done as much or as little as desired
before updating or drawing the chart

    //-- Draw it to the screen
    dc.drawWindowGroups();
    dc.refresh();
    dc.refresh(); //-- SVG issue to get width of text after first
appending
    dc.updateImage( this.$('.image img') );

Sequence of methods to call to successfully update a heavily modified
chart for being redrawn.

    //-- Updating based on user interaction with window or elements
    dc.searchHighlight('Search this term!');
    dc.refresh //-- to handle window resizing
    dc.changeZoom

Methods to call that don't always require a fully redrawing/updating of
the chart

## External APIs

The datachart application relies on 4 service APIs to get the required
information for display:

* [mygene.info/gene/ENTREZ_ID/?filter=homologene](http://mygene.info/gene/1017/?filter=homologene)
* [mygene.info/gene/ENTREZ_ID/?filter=entrezgene,reporter,refseq.rna](http://mygene.info/gene/1017/?filter=entrezgene,reporter,refseq.rna)
* /dataset/DATASET_ID/values/?reporters=CSV_REPORTER_LIST
* /dataset/d3/DATASET_ID/REPORTER_KEY/?format=json

### Old url pattern to redirect to if SVG is not present

[http://biogps.org/plugin/9/gene-expressionactivity-chart/](Plugin: Gene expression/activity chart) url pattern:
http://plugins.biogps.org/data_chart/data_chart.cgi?id=ENTREZ_ID;

### Screenshots

![Saving Drag](https://bitbucket.org/sulab/data-chart-plugin/raw/bd0224a98b53eb2ad16b293487ed5633250c0aaa/assets/screen_shots/6.png)

![Saving Mouse](https://bitbucket.org/sulab/data-chart-plugin/raw/bd0224a98b53eb2ad16b293487ed5633250c0aaa/assets/screen_shots/7.png)

![Adobe Illustrator](https://bitbucket.org/sulab/data-chart-plugin/raw/bd0224a98b53eb2ad16b293487ed5633250c0aaa/assets/screen_shots/8.png)

